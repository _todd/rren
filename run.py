# -*- coding: utf-8 -*-
# python: '3.3.0'

# добавляет или удаляет подстроку в названиях файлов
# в указанной папке и только заданных типов

# 'встроенные' фильтры
# 'all', 'image', 'music', 'video', 'docs', | 'custom'

import renamer

if __name__ == "__main__":

    path = '/home/shuhov/music'
    FILTER = 'music'    # 'all', 'image', 'video', 'docs', 'custom'
    
    #music = renamer.Renamer()
    #music.set_path(path)
    #music.set_filter(filetypes=FILTER, types=('.other1',))     # music.set_filter(types=('.other1', ))
    
    music = renamer.Renamer(path=path, filetypes=FILTER, types=('.other1', ))

    music.add('[MUSIC]')
    # music.undo()
    music.remove('[MUSIC]')

    print('end')