# -*- coding: utf-8 -*-
# python '3.3.0'

import os

# most popular
TYPES = {
    'image': (".bmp", ".jpeg", ".jpg", ".sgv", ".wmf",
              ".png", ".tif", ".tiff", ".gif", ),

    'music': (".mp3", ".flac", ".aac", ".ogg", ".wav", ),

    'video': (".mp4", ".3gp", ".avi", ".mkv", ".ogv", ),

    'doc': (".doc", ".docx", ".odf", ".djvu", ".fb2",
            ".pdf", ".rtf", ".xml", ".xls", ".ppt", ".pptx",
            ".txt", ".html", ".htm", ),

    'all': ("", ),

    'custom': (),
}


class Renamer(object):
    def __init__(self, path="/", filetypes="custom", types=()):

        self.set_path(os.path.abspath(path))
        self.set_filter(filetypes, types)
        
        self.log = {}

    def set_path(self, path):
        if os.path.exists(path) and os.path.isdir(path):
            self.path = os.path.abspath(path)
        else:
            raise RuntimeError("path not found or is not a directory")

    def set_filter(self, filetypes="custom", types=()):
        self.filter = TYPES.get(filetypes, 'custom') + types

    @property
    def filelist(self):
        return (file for file in os.listdir(self.path)
                    if file.lower().endswith(self.filter))

    def add(self, add_item):
        for file in self.filelist:
            if not file.startswith(add_item):
                self.rename(file, add_item+file)

    def remove(self, remove_item):
        for file in self.filelist:
            if remove_item in file:
                self.rename(file, file.replace(remove_item, "", 1))

    def undo(self):
        log = self.log.copy()
        for new in log.keys():
            self.rename(new, log[new])
            del self.log[new]

    def rename(self, old, new):

        old = os.path.join(self.path, old)
        new = os.path.join(self.path, new)

        try:
            os.rename(old, new)
            ret = "\nФайл успешно переименован...\nНовое название: {0}\n".format(new)

        except FileExistsError:
            ret = "\nфайл с таким именем существует.\nТекущее название: {0}\n".format(old)

        except PermissionError:
            ret = "\nОшибка доступа.\nТекущее название: {0}\n".format(old)

        print(old, ret)
        self.log[new] = old